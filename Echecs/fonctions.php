<?php 
function colonnesNames($bd){
	$requete = $bd->prepare('SELECT * FROM joueursEchec');
	$requete -> execute();

	return array_keys($requete->fetch(PDO::FETCH_ASSOC));
}

function entetesTables($bd){
	$res = '<tr>';
	$entetes = colonnesNames($bd);
	foreach($entetes as $key => $value){
		$res = $res.'<th>'.$value.'</th>';
	}

	return $res.'</tr>';

}


function afficheJoueurs($bd){
	$req =$bd->prepare('SELECT * FROM joueursEchec');
	$req->execute();
	$row1 = $req->fetch(PDO::FETCH_ASSOC);
	$columns = array_keys($row1);
	echo '<table><tr><th>'.$columns[0].'</th><th>'.$columns[1].'</th><th>'.$columns[2].'</th><th>'.$columns[3].'</th><th>'.$columns[4].'</th></tr>';
	echo '<tr><td>'.$row1['nom'].'</td><td>'.$row1['prenom'].'</td><td>'.$row1['pays'].'</td><td>'.$row1['score'].'</td><td>'.$row1['naissance'].'</td>';
	echo '<td><a href="supprimer.php?nom='.$row1['nom'].'&prenom='.$row1['prenom'].'"><img src="supprimer.png"></a></td></tr>';


	while($row = $req->fetch(PDO::FETCH_ASSOC)){
		// echo '<tr><td>'.$row->nom.'</td></tr>';
	echo '<tr><td>'.$row['nom'].'</td><td>'.$row['prenom'].'</td><td>'.$row['pays'].'</td><td>'.$row['score'].'</td><td>'.$row['naissance'].'</td>';
	echo '<td><a href="supprimer.php?nom='.$row1['nom'].'&prenom='.$row1['prenom'].'"><img src="supprimer.png"></a></td></tr>';

	}
	echo '</table>';
	// echo '</table>';

}

function scoreMaxMoyen($bd){
	$req_max = $bd->prepare('SELECT MAX(score) FROM joueursEchec');
	$req_max->execute();

	$req_moyen = $bd->prepare('SELECT AVG(score) FROM joueursEchec');
	$req_moyen->execute();

	return array($req_max->fetch(PDO::FETCH_NUM)[0], $req_moyen->fetch(PDO::FETCH_NUM)[0]);

}

function afficheJoueursSelonScore($score, $bd){
	$req_score = $bd->prepare('SELECT * FROM joueursEchec WHERE score=?');
	$req_score->execute(array($score));
	while($player = $req_score->fetch(PDO::FETCH_ASSOC)){
		echo '<p>'.$player['nom'].' '.$player['prenom'];
	}
}

function joueurExiste($nom, $prenom, $bd){
	$req = $bd->prepare('SELECT * FROM joueursEchec WHERE nom=? AND prenom=?;');
	$req->execute(array($nom, $prenom));

	return $req->fetch(PDO::FETCH_ASSOC) != null;
}

function testCles($tab_post){
	$res = True;
	$tab_cles = array('nom', 'prenom', 'pays', 'score', 'naissance');
	// $res = $res && count($tab_cles) == count($tab_post);
	foreach ($tab_cles as $key => $value) {
		$res = $res && isset($tab_post[$value]) && (trim($tab_post[$value]) != '');
	}
	return $res;
}

function updateJoueur($joueur, $bd){
	$req = $bd -> prepare("UPDATE joueursEchec SET score =? , pays=?, naissance=? WHERE nom=? AND prenom=?");
	$req -> execute(array($joueur['score'],$joueur['pays'],$joueur['naissance'],$joueur['nom'],$joueur['prenom'] ));
	
}

function ajoutJoueur($joueur, $bd){
	$req = $bd -> prepare("INSERT INTO joueursEchec VALUES (?, ?, ?, ?, ?)");
	$req -> execute(array_values($joueur));
	
}

function afficheSelonFiltre($filtres, $ordre, $bd){
	$requete = 'SELECT * FROM joueursEchec WHERE';
	if (isset($filtres['pays'])) // ajout pays a la requete
		$requete = $requete . ' pays=:pays';
	if (isset($filtres['naissance'])) // ajout naissance a la requete
		$requete = $requete . (isset($filtres['pays'])?'and':'') .' naissance '.($filtres['signeNaissance'] == '-1'?' <= :naissance' 
			: ($filtres['signeNaissance'] == '0'? ' = :naissance': '>= :naissance') );
	if (isset($ordre)){
		if ($ordre == 'nom')
			$requete = $requete . ' ORDER BY '.$ordre.', prenom';
		else
			$requete = $requete . ' ORDER BY score, prenom';
	}
	
	$req = $bd->prepare($requete);
	if (isset($filtres['pays']))
		$req->bindValue(':pays', $filtres['pays']);
	if (isset($filtres['naissance']))
		$req -> bindValue(':naissance', $filtres['naissance']);
	

	$req->execute();
	$row1 = $req->fetch(PDO::FETCH_ASSOC);
	$columns = array_keys($row1);
	echo '<table><tr><th>'.$columns[0].'</th><th>'.$columns[1].'</th><th>'.$columns[2].'</th><th>'.$columns[3].'</th><th>'.$columns[4].'</th></tr>';
	echo '<tr><td>'.$row1['nom'].'</td><td>'.$row1['prenom'].'</td><td>'.$row1['pays'].'</td><td>'.$row1['score'].'</td><td>'.$row1['naissance'].'</td>';
	echo '<td><a href="supprimer.php?nom='.$row1['nom'].'&prenom='.$row1['prenom'].'"><img src="supprimer.png"></a></td></tr>';


	while($row = $req->fetch(PDO::FETCH_ASSOC)){
		// echo '<tr><td>'.$row->nom.'</td></tr>';
	echo '<tr><td>'.$row['nom'].'</td><td>'.$row['prenom'].'</td><td>'.$row['pays'].'</td><td>'.$row['score'].'</td><td>'.$row['naissance'].'</td>';
	echo '<td><a href="supprimer.php?nom='.$row["nom"].'&prenom='.$row["prenom"].'"><img src="supprimer.png"></a></td></tr>';

	}
	echo '</table>';
}

function genereOptionsSelectPays($bd){
	$req = $bd -> prepare('SELECT DISTINCT(pays) FROM joueursEchec ORDER BY pays ASC');
	$req -> execute();

	$res = '';

	while($row = $req->fetch(PDO::FETCH_NUM)){
		$res = $res. "<option value=".$row[0].">".ucfirst($row[0])."</option>";
	}

	echo $res;
}

function genereURL($key, $val){
	$tab = $_GET;
	$tab[$key] = $val;
	$url = $_SERVER['PHP_SELF']."?";
	$url = $url.http_build_query($tab);
	return $url;
	
}

function requeteFiltrer($filtres){
	$where = 'WHERE';
	if (isset($filtres['signeNaissance'])){
		$signe = ($filtres['signeNaissance'] == '0' ? '=': ($filtres['signeNaissance'] == '1' ? '>=': '<='));
		unset($filtres['signeNaissance']);
	}
	$ind = 0;
	foreach ($filtres as $k => $v) {
		$where = $where.($ind == 0 ? '': ' AND').' '.$k.($k=='naissance' ? $signe : '=').(gettype($v) == 'string' ? '"'.$v.'"': $v);
	}

	return $where;
}

function afficheParPagesAvecFiltres($filtres, $ordre, $page, $nombres, $bd){
	$tableau = '<table>'.entetesTables($bd);


	$limit_basse =($page - 1) * $nombres;

	$requete='SELECT * FROM joueursEchec ';
	$reqCount = 'SELECT COUNT(*) FROM joueursEchec ';

	$reqfiltres='';

	if (isset($filtres) && $filtres != Array()){
		$reqfiltres = requeteFiltrer($filtres);
	}
	$reqCount = $reqCount . $reqfiltres;
	$requete = $requete . $reqfiltres;

	$reqCount = $bd -> prepare($reqCount);
	$reqCount->execute();

	$count = $reqCount->fetch(PDO::FETCH_NUM);
	$nbPages = $count[0] / $nombres + ($count[0] % $nombres == 0 ? 0 : 1);

	

	if (isset($ordre)){
		if ($ordre == 'nom')
			$requete = $requete . ' ORDER BY '.$ordre.', prenom';
		else
			$requete = $requete . ' ORDER BY score';
	}


	$requete = $bd -> prepare($requete.' LIMIT :limit_basse, :nb');
	$requete -> bindValue(':limit_basse', $limit_basse, PDO::PARAM_INT);
	$requete -> bindValue(':nb', (int)$nombres, PDO::PARAM_INT);

	$requete -> execute();

	while($row = $requete->fetch(PDO::FETCH_ASSOC)){
		$tableau = $tableau . '<tr><td>'.$row['nom'].'</td><td>'.$row['prenom'].'</td><td>'.$row['pays'].'</td><td>'.$row['score'].'</td><td>'.$row['naissance'].'</td>'.
		'<td class="sansBordure"><a href="supprimer.php?nom='.$row["nom"].'&prenom='.$row["prenom"].'"><img src="supprimer.png"></a></td></tr>';

	}

	echo $tableau.'</table>';

	$txtPages = '<ul style="list-style-type: none">';


	foreach (range(1, $nbPages) as $value) {
		$txtPages = $txtPages . '<li style="display: inline'. ($value == $page ? ';color: black"': '; color: grey"').'><a href="'. genereURL('page', $value).'">'.$value.'</a>  </li>';
	}

	echo $txtPages.'</ul>';


}

 ?>