<?php 
	require('connexion.php'); //Permet la connexion à la base de données
	require('debut.php'); //En-tête html
	require('fonctions.php'); //Ensemble de fonctions php
?>
<section class="resultat">
<?php 
if ((isset($_GET['nom']) && trim($_GET['nom'] != "")) && (isset($_GET['prenom']) && trim($_GET['prenom'])))
	if (joueurExiste($_GET['nom'], $_GET['prenom'], $bd)){
?>
		<form action="<?php echo 'liste.php'.http_build_query($_POST); ?>" method="post">
			<p> Voulez-vous vraiment supprimer le joueur <?php echo $_GET['nom'].' '.$_GET['prenom']; ?> ?
				<input type="hidden" name="nom" value="<?= $_GET['nom'] ?>">
				<input type="hidden" name="prenom" value="<?= $_GET['prenom'] ?>">
			</p>
			<p>
				<input type="submit" value="oui" name="suppression">
				<input type="submit" value="non" name="suppression">
			</p>
		</form>
<?php } 
	else {?>
		<p>Erreur: Le joueurs <?php echo $_GET['nom'] ?> <?php echo $_GET['prenom'] ?> n'éxiste pas. <br> <br>
		<a href="liste.php">Retour liste</a>.
		</p>
<?php } ?>

</section>

<?php  require('fin.php'); ?>

