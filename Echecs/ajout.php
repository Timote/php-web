<?php 
	require('connexion.php'); //Permet la connexion à la base de données
	require('debut.php'); //En-tête html
	require('fonctions.php'); //Ensemble de fonctions php
?>


<?php 
if (testCles($_POST) && (int) $_POST['score'] >= 0 && (int) $_POST['naissance'] >=1900 && (int) $_POST['naissance'] <= 2010) {
	echo "<section class='filtre'>";
	if (joueurExiste($_POST['nom'], $_POST['prenom'], $bd)) {
		updateJoueur($_POST, $bd);
	}
	else
		ajoutJoueur($_POST, $bd);
	echo "Mise à jour de la base de donnée effectuée";
	echo "</section>";
} 
?>



<section class="resultat">

<h2> Ajout ou modification d'un joueur </h2>
<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
<p><label> Nom : </label> <input type="text" name="nom" id="nom"/></p>
<p><label> Prénom : </label> <input type="text" name="prenom" id="prenom"/></p>
<p><label> Pays : </label><input type="text" name="pays" id="pays"/></p>
<p><label> Score : </label> <input type="text" name="score" id="score"/></p>
<p><label> Naissance : </label> <input type="text" name="naissance" id="naissance"/></p>
<p> <input type="submit" /> </p>
</form>
</section>




<?php require('fin.php'); ?>
