<?php 
	require('connexion.php'); //Permet la connexion à la base de données
	require('debut.php'); //En-tête html
	require('fonctions.php'); //Ensemble de fonctions php
?>

<?php 
if (isset($_POST['prenom']) && isset($_POST['nom']) && isset($_POST['suppression']) && $_POST['suppression']=='oui' && joueurExiste($_POST['nom'], $_POST['prenom'], $bd)){
    $requete = $bd->prepare("DELETE FROM joueursEchec WHERE nom=:nom AND prenom=:prenom");
    $requete -> bindValue(':nom', $_POST['nom']);
    $requete -> bindValue(':prenom', $_POST['prenom']);
    $requete -> execute();
}

$filtres = array();
if (isset($_GET['naissance']) && $_GET['naissance'] >= 1900 && $_GET['naissance'] <= 2010 
	&& isset($_GET['signeNaissance']) && $_GET['signeNaissance'] >= -1 && $_GET['signeNaissance'] <= 1) {
	$filtres['naissance'] = $_GET['naissance'];
	$filtres['signeNaissance'] = $_GET['signeNaissance'];
} 

if (isset($_GET['pays']) && trim($_GET['pays']) != '' && $_GET['pays'] != 'tous')
	$filtres['pays'] = $_GET['pays'];

if (isset($_GET['ordre']) && trim($_GET['ordre']) != '')
    $ordre=$_GET['ordre'];
else
    $ordre='score';

if (isset($_GET['page']) && trim($_GET['page']) != '')
    $page = $_GET['page'];
else
    $page = 1;

if (isset($_GET['nbRow']) && trim($_GET['nbRow']) != '')
    $nbRow = $_GET['nbRow'];
else
    $nbRow = 10; 


?>

<section class="filtre">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <p> Annee de naissance : 
        <select name="signeNaissance">
           <option value="-1"> &le; </value>
           <option value="0"> == </value>
           <option value="1"> &ge; </value>
        </select>
        <input type="text" name="naissance"/> Pays :
        <select name="pays">
            <option value="tous" selected="selected"> --- </option>
            <?php genereOptionsSelectPays($bd); ?>
        </select> 

        <select name="nbRow">

            <option value="10" <?php echo ((isset($_GET['nbRow']) && (int)$_GET['nbRow'] == 10 ) ? 'selected': '') ?> > 10 </option>
            <option value="20" <?php echo ((isset($_GET['nbRow']) && (int)$_GET['nbRow'] == 20 ) ? 'selected': '') ?> > 20 </option>
            <option value="50" <?php echo ((isset($_GET['nbRow']) && (int)$_GET['nbRow'] == 50 ) ? 'selected': '') ?> > 50 </option>
        <input type="hidden" name="ordre" value="<?php echo $ordre; ?>"/>
        <input type="submit" value="Filtrer"/></p>
    </form>
    <p class="droite">Trier par : <a href="<?php echo genereURL('ordre', 'score'); ?>">score</a>  <a href="<?php echo genereURL('ordre', 'nom'); ?>">nom</a></p>
</section>
<section class="resultat">


<!-- Afficher la liste de tous les joueurs sous forme de tableau html-->
<!-- Ceci doit être fait en se connectant à la base de données -->

<?php //($filtres != array()) ? afficheSelonFiltre($filtres,$ordre,$bd) : afficheJoueurs($bd); ?> 

<?php afficheParPagesAvecFiltres($filtres, $ordre, $page, $nbRow, $bd) ?>



</section>

<?php require('fin.php'); ?>
