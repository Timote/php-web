<?php 
	require('connexion.php'); //Permet la connexion à la base de données
	require('debut.php'); //En-tête html
	require('fonctions.php'); //Ensemble de fonctions php
?>




<section class="resultat">
<?php $values = scoreMaxMoyen($bd); ?>
<p>Meilleurs joueurs d'échecs au monde (ayant atteint le score <?php echo $values[0]; ?>) :</p>

<!-- Affichage de tous les joueurs ayant le meilleur score -->
<?php afficheJoueursSelonScore($values[0], $bd); ?>

<p>Score moyen au classement ELO : <?php echo $values[1]; ?> </p>

<!-- <?php echo joueurExiste('Nii', 'Hua', $bd)? 'Existe': 'Existe pas'; ?> -->

</section>

<?php require('fin.php'); ?>
